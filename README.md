##### Git全局设置

```ja
git config --global user.name "qinyexiong"
git config --global user.email "1093998509@qq.com"
```

##### 创建一个新的仓库qq+asdad

```
git clone git@gitlab.com:qinyexiong/Ri_Ji.git
cd Ri_Ji
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

##### 推送现有文件夹

```
cd existing_folder
git init
git remote add origin git@gitlab.com:qinyexiong/Ri_Ji.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

##### 推送现有的Git存储库

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:qinyexiong/Ri_Ji.git
git push -u origin --all
git push -u origin --tags
```